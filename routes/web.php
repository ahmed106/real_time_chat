<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::controller(\App\Http\Controllers\MessageController::class)->middleware('auth')->group(function (){
    Route::get('/send/{user}','chat');
    Route::post('/send','send')->name('send');
});

//Route::get('/send/{user}/{message}',function (\App\Models\User $user,$message){
//
//
//   event(new \App\Events\Chat_two($message,$user->id));
//    return 'success';
//})->middleware('auth');


Route::get('receive-message',function (){

    return view('receive');
})->middleware('auth');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
