<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $table = 'messages';
    protected $guarded = [];

   public function fromUser()
   {
       return $this->hasOne(User::class,'id','from_user_id');

   }//end of fromUser

    public function toUser()
    {
        return $this->hasOne(User::class,'id','to_user_id');

    }//end of fromUser
}
