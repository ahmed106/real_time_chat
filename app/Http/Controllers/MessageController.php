<?php

namespace App\Http\Controllers;

use App\Events\Chat_two;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;

class MessageController extends Controller
{
   public function chat(User $user)
   {
      $messages = Message::with(['fromUser','toUser'])
          ->where('to_user_id',auth()->user()->id)
          ->orWhere('from_user_id',auth()->user()->id)
          ->get();

      return view('chat',compact('user','messages'));
   }//end of index

    public function send(Request $request)
    {
     $message =Message::create([
            'message'=>$request->message,
            'is_read'=>0,
            'from_user_id'=>auth()->id(),
            'to_user_id'=>$request->to_user,
        ]);


        event(new Chat_two($message));
    }//end of send
}
